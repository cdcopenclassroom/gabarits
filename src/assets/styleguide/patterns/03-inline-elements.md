## Éléments inline

### A (anchor)

L'élément `<a>`&nbsp;définit un hyperlien, une cible de destination nommée pour un hyperlien, ou les deux à la fois.

    @example
    <p>C'est que sa planète d'origine était à peine <a href="#">plus grande qu'une maison</a>.</p>

### Strong

L'élément `<strong>`&nbsp;est utilisé pour donner de l'importance à un texte.

    @example
    <p>C'est que sa planète d'origine était à peine <strong>plus grande qu'une maison</strong>.</p>

### Em (emphase)

L'élément `<em>`&nbsp;sert à marquer un texte sur lequel on veut insister.

    @example
    <p>C'est que <em>sa planète d'origine</em>&nbsp;était à peine plus grande qu'une maison.</p>

### Small

L'élément `<small>`&nbsp;permet de représenter du texte avec une police dont la taille est plus petite que celle utilisée pour le texte environnant.

    @example
    <p>C'est que <small>sa planète d'origine</small>&nbsp;était à peine plus grande qu'une maison.</p>


### S (Strikethrough)

L'élément `<s>`&nbsp;permet d'afficher du texte barré au sens où celui-ci n'est plus d'actualité ou n'est plus pertinent.

    @example
    <p>C'est que <s>sa nouvelle planète</s>&nbsp;sa planète d'origine était à peine plus grande qu'une maison.</p>


### Mark

L'élément `<mark>`&nbsp;représente du texte surligné, c'est-à-dire du texte marqué afin d'être utilisé en tant que source étant donné sa pertinence dans un contexte en particulier.

    @example
    <p>C'est que sa <mark>planète</mark>&nbsp;d'origine était à peine plus grande qu'une maison.</p>


### Abbr

L'élément `<abbr>`&nbsp;représente une abréviation et permet de façon optionnelle d'en fournir une description complète. S'il est présent, l'attribut title doit contenir cette même description complète et rien d'autre. S'il y a plusieurs occurences de la même abréviation dans une page, l'attribut title ne doit être présent que sur la 1ère occurence.

    @example
    <p>Ce document a été conçu avec du code <abbr title="HyperText Markup Language">HTML</abbr>. Voir la définition de
    <abbr>HTML</abbr>
    sur <a href="https://fr.wikipedia.org/wiki/Hypertext_Markup_Language">Wikipedia</a>.</p>
