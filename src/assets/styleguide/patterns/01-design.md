## Design

### Palette de Couleurs

  <div class="sg-example"><div class="sg-canvas">
      <div class="sg-color"> <!-- number of items = number of colors -->
        <div class="sg-color-item"></div>
        <div class="sg-color-item"></div>
        <div class="sg-color-item"></div>
        <div class="sg-color-item"></div>
        <div class="sg-color-item"></div>
        <div class="sg-color-item"></div>
        <div class="sg-color-item"></div>
        <div class="sg-color-item"></div>
        <div class="sg-color-item"></div>
      </div>
  </div></div>
