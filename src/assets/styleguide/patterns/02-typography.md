## Typographie

### Fonts

  <div class="sg-example"><div class="sg-canvas">
      <div class="sg-font-common">
          <code>$font-stack-common</code><br>
          ABCDEFGHIJKLMNOPQRSTUVWXYZ<br>
          abcdefghijklmnopqrstuvwxyz<br>
          0123456789
      </div>
      <div class="sg-font-headings">
          <code>$font-stack-headings</code><br>
          ABCDEFGHIJKLMNOPQRSTUVWXYZ<br>
          abcdefghijklmnopqrstuvwxyz<br>
          0123456789
      </div>
      <div class="sg-font-monospace">
          <code>$font-stack-monospace</code><br>
          ABCDEFGHIJKLMNOPQRSTUVWXYZ<br>
          abcdefghijklmnopqrstuvwxyz<br>
          0123456789
      </div>
  </div></div>

### Titres

Niveaux de titre, de `<h1>`&nbsp;à `<h6>`.

    @example
    <h1>Titre de niveau 1</h1>
    <h2>Titre de niveau 2</h2>
    <h3>Titre de niveau 3</h3>
    <h4>Titre de niveau 4</h4>
    <h5>Titre de niveau 5</h5>
    <h6>Titre de niveau 6</h6>

### Texte

Paragraphe avec des éléments <i>inline</i>.

    @example
    <p>J'avais ainsi appris une seconde chose très importante : C'est que sa planète d'origine était à peine <em>plus grande qu'une maison</em>&nbsp;!</p>
    <p>Ça ne pouvait pas m'étonner beaucoup. Je savais bien qu'en dehors des <strong>grosses planètes comme la Terre</strong>, Jupiter, Mars, Vénus, auxquelles on a donné des noms, il y en a des centaines d'autres qui sont quelquefois si petites qu'on a beaucoup de mal à les apercevoir au télescope. 
    Quand un astronome découvre l'une d'elles, il lui donne pour nom un numéro. Il l'appelle par exemple : <a href="#">"l'astéroïde 3251"</a>.</p>
