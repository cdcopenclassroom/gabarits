$(document).ready(function(){
    $('.tab  .menu').on('click', function(event){
        event.preventDefault();

        $('.tab  .menu').removeClass('active');
        $(this).addClass('active');

        var contanchor = $(this).attr('href');
        $('.tab-container > div').removeClass('active');
        $(contanchor).addClass('active');
    })
  });